part of 'shared.dart';

const String userFirestoreCollection = "user";

const String userEmailData = "email";
const String userNameData = "name";
const String userProfilePictureData = "profilePicture";
const String userSelectedGenresData = "selectedGenres";
const String userSelectedLanguageData = "selectedLanguage";
const String userBalanceData = "balance";

const String localeID = "id_ID";

const String alertEmailAlreadyUse =
    "Email Already Use, please change to another email or just continue sign in";
const String alertFormatEmailAddress = "Wrong formatted email address";
const String alertPassMoreThan = "Password must more than 6 character";
const String alertMissmatchPassword =
    "Mismatch password and confirmed password";
const String alertFillallField = "Please fill all the fields";
const String alertSelectFourGenres = "Please select 4 genres";
