part of 'shared.dart';

Future<File?> getImage() async {
  var xFile = await ImagePicker().pickImage(source: ImageSource.gallery);
  if ((xFile ?? "") == "") {
    return null;
  }
  return File(xFile!.path);
}

Future<String> uploadImage(File image) async {
  String filename = basename(image.path);

  firebase_storage.FirebaseStorage firebaseStorage =
      firebase_storage.FirebaseStorage.instance;
  firebase_storage.Reference reference = firebaseStorage.ref().child(filename);
  firebase_storage.UploadTask task = reference.putFile(image);
  firebase_storage.TaskSnapshot snapshot = await task;

  return await snapshot.ref.getDownloadURL();
}
