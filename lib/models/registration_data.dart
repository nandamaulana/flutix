part of 'models.dart';

class RegistrationData {
  String? name = "";
  String? email = "";
  String? password = "";
  List<String>? selectedGenres = const [];
  String? selectedLang = "";
  File? profileImage;

  RegistrationData({
    this.name,
    this.email,
    this.password,
    this.selectedGenres,
    this.selectedLang,
    this.profileImage,
  });
}
