part of 'widgets.dart';

class SelectableBox extends StatelessWidget {
  final bool isSelected;
  final bool isEnabled;
  final double height;
  final double width;
  final String text;
  final Function? onTap;
  final TextStyle? textStyle;

  const SelectableBox({
    Key? key,
    this.isSelected = false,
    this.isEnabled = true,
    this.height = 144,
    this.width = 60,
    required this.text,
    this.onTap,
    this.textStyle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.all(Radius.circular(6)),
      color: (!isEnabled)
          ? mercury
          : isSelected
              ? accentColor2
              : Colors.transparent,
      child: InkWell(
        onTap: () {
          if ((onTap ?? "") != "") onTap!();
        },
        child: Container(
          width: width,
          height: height,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
              border: Border.all(
                  color: (!isEnabled)
                      ? mercury
                      : isSelected
                          ? Colors.transparent
                          : mercury)),
          child: Center(
            child: Text(text,
                style: (textStyle ?? blackTextFont)
                    .copyWith(fontSize: 16, fontWeight: FontWeight.w400)),
          ),
        ),
      ),
    );
  }
}
