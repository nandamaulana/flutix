part of 'pages.dart';

class MoviePage extends StatelessWidget {
  const MoviePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Container(
          height: 134,
          decoration: BoxDecoration(
              color: accentColor1,
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20))),
          padding: EdgeInsets.fromLTRB(defaultMargin, 20, defaultMargin, 30),
          child: BlocBuilder<UserBloc, UserState>(
            builder: (_, state) {
              if (state is UserLoaded) {
                if (imageFileToUpload != null) {
                  uploadImage(imageFileToUpload!).then((downloadUrl) {
                    imageFileToUpload = null;
                    context
                        .read<UserBloc>()
                        .add(UpdateData(profileImage: downloadUrl));
                  });
                }
                return Row(
                  children: [
                    Container(
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: butterflyBush),
                        ),
                        child: Stack(
                          children: [
                            Align(
                              alignment: Alignment.center,
                              child: SpinKitFadingCircle(
                                  color: accentColor2, size: 25),
                            ),
                            Align(
                              alignment: Alignment.center,
                              child: Container(
                                  width: 50,
                                  height: 50,
                                  decoration: BoxDecoration(
                                      color: Colors.amber,
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                          fit: BoxFit.cover,
                                          image: ((state.user.profilePicture ??
                                                      "") ==
                                                  "")
                                              ? AssetImage(
                                                      "assets/user_pic.png")
                                                  as ImageProvider
                                              : NetworkImage(state
                                                  .user.profilePicture!)))),
                            ),
                          ],
                        )),
                    SizedBox(
                      width: 16,
                    ),
                    Flexible(
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              (state.user.name ?? "") != ""
                                  ? state.user.name!
                                  : "",
                              style: whiteTextFont.copyWith(fontSize: 18),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                            Text(
                                NumberFormat.currency(
                                        locale: localeID,
                                        decimalDigits: 0,
                                        symbol: "IDR ")
                                    .format(state.user.balance),
                                style: yellowNumberFont.copyWith(
                                    fontSize: 14, fontWeight: FontWeight.w400))
                          ]),
                    )
                  ],
                );
              } else {
                return SpinKitFadingCircle(color: accentColor2, size: 50);
              }
            },
          ),
        ),
        Container(
          margin: EdgeInsets.only(
              left: defaultMargin, right: defaultMargin, top: 30, bottom: 12),
          child: Text(
            "Now Playing",
            style: blackTextFont.copyWith(
                fontSize: 18, fontWeight: FontWeight.bold),
          ),
        )
      ],
    );
  }
}
