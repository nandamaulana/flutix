part of 'pages.dart';

class AccountConfirmationPage extends StatefulWidget {
  final RegistrationData registrationData;
  const AccountConfirmationPage(this.registrationData, {Key? key})
      : super(key: key);

  @override
  State<AccountConfirmationPage> createState() =>
      _AccountConfirmationPageState();
}

class _AccountConfirmationPageState extends State<AccountConfirmationPage> {
  bool isSignUp = false;
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          context
              .read<PageBloc>()
              .add(GoToPreferencePage(widget.registrationData));
          return Future.value(false);
        },
        child: Scaffold(
            body: Container(
                color: Colors.white,
                padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                child: ListView(
                  children: [
                    Column(children: [
                      Container(
                        height: 56,
                        margin: EdgeInsets.only(top: 20, bottom: 22),
                        child: Stack(
                          children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Material(
                                color: Colors.transparent,
                                child: InkWell(
                                  customBorder: CircleBorder(),
                                  onTap: () {
                                    context.read<PageBloc>().add(
                                        GoToPreferencePage(
                                            widget.registrationData));
                                  },
                                  child: Icon(Icons.arrow_back),
                                ),
                              ),
                            ),
                            Align(
                                alignment: Alignment.center,
                                child: Text("Create New \nYour Account",
                                    style: blackTextFont.copyWith(
                                        fontSize: 20,
                                        fontWeight: FontWeight.w600),
                                    textAlign: TextAlign.center)),
                          ],
                        ),
                      ),
                      Container(
                          width: 150,
                          height: 150,
                          margin: EdgeInsets.only(bottom: 20),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image:
                                      ((widget.registrationData.profileImage ??
                                                  "") ==
                                              "")
                                          ? AssetImage("assets/user_pic.png")
                                              as ImageProvider
                                          : FileImage(widget
                                              .registrationData.profileImage!),
                                  fit: BoxFit.cover))),
                      Text("Welcome",
                          style: blackTextFont.copyWith(
                              fontSize: 20, fontWeight: FontWeight.w500))
                    ]),
                    Text(
                      "${widget.registrationData.name}",
                      textAlign: TextAlign.center,
                      style: blackTextFont.copyWith(
                          fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 110,
                    ),
                    (isSignUp)
                        ? SpinKitFadingCircle(
                            color: mainColor,
                          )
                        : SizedBox(
                            width: 250,
                            height: 45,
                            child: ElevatedButton(
                                onPressed: () async {
                                  setState(() {
                                    isSignUp = true;
                                  });

                                  imageFileToUpload =
                                      widget.registrationData.profileImage;

                                  SignInSignUpResult result =
                                      await AuthServices.signUp(
                                          widget.registrationData.email!,
                                          widget.registrationData.password!,
                                          widget.registrationData.name!,
                                          widget
                                              .registrationData.selectedGenres!,
                                          widget
                                              .registrationData.selectedLang!);

                                  if (result.user == null) {
                                    setState(() {
                                      isSignUp = false;
                                    });

                                    result.message
                                        .showMessage(context: context);
                                  }
                                },
                                style: ElevatedButton.styleFrom(
                                  primary: keppel,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                ),
                                child: Text(
                                  "Create My Account",
                                  style: whiteTextFont.copyWith(fontSize: 16),
                                )))
                  ],
                ))));
  }
}
