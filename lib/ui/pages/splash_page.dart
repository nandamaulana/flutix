part of 'pages.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: defaultMargin),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 136,
              decoration: BoxDecoration(
                  image: DecorationImage(image: AssetImage("assets/logo.png"))),
            ),
            Container(
              margin: EdgeInsets.only(top: 70, bottom: 16),
              child: Text(
                "New Experience",
                style: blackTextFont.copyWith(fontSize: 20),
              ),
            ),
            Text("Watch a movie much\n easier than any before",
                textAlign: TextAlign.center,
                style: greyTextFont.copyWith(
                    fontSize: 16, fontWeight: FontWeight.w300)),
            Container(
                width: 250,
                height: 46,
                margin: EdgeInsets.only(top: 70, bottom: 19),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: mainColor),
                  child: Text("Get Started",
                      style: whiteTextFont.copyWith(fontSize: 16)),
                  onPressed: () {
                    context
                        .read<PageBloc>()
                        .add(GoToSignUpPage(RegistrationData()));
                  },
                )),
            Text.rich(TextSpan(children: [
              TextSpan(
                  text: "Already Have an Account? ",
                  style: greyTextFont.copyWith(fontWeight: FontWeight.w400)),
              WidgetSpan(
                  child: GestureDetector(
                onTap: () async {
                  context.read<PageBloc>().add(GoToLoginPage());
                },
                child: Text("Sign In", style: purpleTextFont),
              ))
            ]))
          ],
        ),
      ),
    );
  }
}
