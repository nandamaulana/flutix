part of 'pages.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  bool isEmailValid = false;
  bool isPasswordValid = false;
  bool isSigningIn = false;

  @override
  Widget build(BuildContext context) {
    context.read<ThemeBloc>().add(ChangeTheme(ThemeData().copyWith(
        inputDecorationTheme: InputDecorationTheme(
            labelStyle: TextStyle(color: Colors.grey),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: accentColor2))))));

    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(GoToSplashPage());
        return false;
      },
      child: Scaffold(
          backgroundColor: Colors.white,
          body: Container(
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: ScrollConfiguration(
              behavior: NoScrollGlowBehavior(),
              child: ListView(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 30,
                      ),
                      SizedBox(
                        height: 70,
                        child: Image.asset("assets/logo.png"),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 70, bottom: 40),
                        child: Text("Welcome Back,\nExplorer!",
                            style: blackTextFont.copyWith(fontSize: 20)),
                      ),
                      TextField(
                          controller: emailController,
                          onChanged: (text) {
                            setState(() {
                              isEmailValid = EmailValidator.validate(text);
                            });
                          },
                          decoration: InputDecoration(
                              focusColor: accentColor2,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              labelText: "Email Address",
                              hintText: "Email Address")),
                      SizedBox(
                        height: 16,
                      ),
                      TextField(
                          controller: passwordController,
                          onChanged: (text) {
                            setState(() {
                              isPasswordValid = text.length >= 6;
                            });
                          },
                          obscureText: true,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              labelText: "Password",
                              hintText: "Password")),
                      SizedBox(
                        height: 6,
                      ),
                      Text.rich(TextSpan(children: [
                        TextSpan(
                            text: "Forgot Password? ",
                            style: greyTextFont.copyWith(
                                fontWeight: FontWeight.w400)),
                        WidgetSpan(
                            child: GestureDetector(
                          onTap: () {
                            context.read<PageBloc>().add(GoToLoginPage());
                          },
                          child: Text("Get Now", style: purpleTextFont),
                        ))
                      ])),
                      Center(
                        child: Container(
                          width: 50,
                          height: 50,
                          margin: EdgeInsets.only(top: 40, bottom: 30),
                          child: isSigningIn
                              ? SpinKitFadingCircle(
                                  color: mainColor,
                                )
                              : FloatingActionButton(
                                  elevation: 0,
                                  child: Icon(Icons.arrow_forward,
                                      color: isEmailValid && isPasswordValid
                                          ? Colors.white
                                          : silver),
                                  backgroundColor:
                                      isEmailValid && isPasswordValid
                                          ? mainColor
                                          : mercury,
                                  onPressed: isEmailValid && isPasswordValid
                                      ? () async {
                                          setState(() {
                                            isSigningIn = true;
                                          });
                                          SignInSignUpResult result =
                                              await AuthServices.signIn(
                                                  emailController.text,
                                                  passwordController.text);
                                          if (result.user == null) {
                                            setState(() {
                                              isSigningIn = false;
                                            });
                                            Flushbar(
                                              duration: Duration(seconds: 4),
                                              flushbarPosition:
                                                  FlushbarPosition.TOP,
                                              backgroundColor: wildWatermelon,
                                              message: result.message,
                                            )..show(context);
                                          }
                                        }
                                      : null,
                                ),
                        ),
                      ),
                      Text.rich(TextSpan(children: [
                        TextSpan(
                            text: "Start Fresh Now? ",
                            style: greyTextFont.copyWith(
                                fontWeight: FontWeight.w400)),
                        WidgetSpan(
                            child: GestureDetector(
                          onTap: () {
                            context.read<PageBloc>().add(GoToLoginPage());
                          },
                          child: Text("Sign Up", style: purpleTextFont),
                        ))
                      ])),
                    ],
                  ),
                ],
              ),
            ),
          )),
    );
  }
}
