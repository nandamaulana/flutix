part of 'pages.dart';

class PreferencePage extends StatefulWidget {
  final RegistrationData registrationData;
  final List<String> genres = [
    "Horror",
    "Music",
    "Action",
    "Drama",
    "War",
    "Crime"
  ];
  final List<String> languages = ["Bahasa", "English", "Japanese", "Korean"];
  PreferencePage({Key? key, required this.registrationData}) : super(key: key);

  @override
  _PreferencePageState createState() => _PreferencePageState();
}

class _PreferencePageState extends State<PreferencePage> {
  List<String> selectedGenres = [];
  String selectedLanguage = "English";

  @override
  void initState() {
    super.initState();
    selectedGenres = (widget.registrationData.selectedGenres ?? []);
    selectedLanguage = (widget.registrationData.selectedLang ?? "English");
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        widget.registrationData.password = "";
        context.read<PageBloc>().add(GoToSignUpPage(widget.registrationData));
        return Future.value(false);
      },
      child: Scaffold(
          body: Container(
        color: Colors.white,
        padding: EdgeInsets.symmetric(horizontal: defaultMargin),
        child: ListView(
          children: [
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Container(
                height: 56,
                margin: EdgeInsets.only(top: 20, bottom: 22),
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                    customBorder: CircleBorder(),
                    onTap: () {
                      widget.registrationData.password = "";
                      context
                          .read<PageBloc>()
                          .add(GoToSignUpPage(widget.registrationData));
                    },
                    child: Icon(Icons.arrow_back),
                  ),
                ),
              ),
              Container(
                  child: Text("Select Your Four \nFavorite Genres",
                      style: blackTextFont.copyWith(
                          fontSize: 20, fontWeight: FontWeight.w600),
                      textAlign: TextAlign.start)),
              SizedBox(
                height: 16,
              ),
              Wrap(
                spacing: 24,
                runSpacing: 24,
                children: generateGenreAndLanguageWidgets(context,
                    dataList: widget.genres, onTap: (e) {
                  if (selectedGenres.contains(e)) {
                    selectedGenres.remove(e);
                  } else {
                    if (selectedGenres.length < 4) {
                      selectedGenres.add(e);
                    }
                  }
                  setState(() {});
                }),
              ),
              SizedBox(
                height: 16,
              ),
              Container(
                  child: Text("Movie language \nYou Prefer ",
                      style: blackTextFont.copyWith(
                          fontSize: 20, fontWeight: FontWeight.w600),
                      textAlign: TextAlign.start)),
            ]),
            SizedBox(
              height: 16,
            ),
            Wrap(
              spacing: 24,
              runSpacing: 24,
              children: generateGenreAndLanguageWidgets(context,
                  type: "language", dataList: widget.languages, onTap: (e) {
                setState(() {
                  selectedLanguage = e;
                });
              }),
            ),
            SizedBox(
              height: 30,
            ),
            FloatingActionButton(
              elevation: 0,
              child: Icon(Icons.arrow_forward,
                  color: selectedGenres.length == 4 ? Colors.white : silver),
              backgroundColor: selectedGenres.length == 4 ? mainColor : mercury,
              onPressed: selectedGenres.length == 4
                  ? () async {
                      widget.registrationData.selectedGenres = selectedGenres;
                      widget.registrationData.selectedLang = selectedLanguage;
                      context.read<PageBloc>().add(
                          GoToAccountConfirmationPage(widget.registrationData));
                    }
                  : () {
                      alertSelectFourGenres.showMessage(context: context);
                    },
            ),
            SizedBox(
              height: 50,
            )
          ],
        ),
      )),
    );
  }

  List<Widget> generateGenreAndLanguageWidgets(BuildContext context,
      {String type = "genres",
      required List<String> dataList,
      required Function(String) onTap}) {
    double width =
        (MediaQuery.of(context).size.width - (2 * defaultMargin) - 24) / 2;
    return dataList
        .map((e) => SelectableBox(
              text: e,
              width: width,
              height: 60,
              isSelected: type == "genres"
                  ? selectedGenres.contains(e)
                  : selectedLanguage == e,
              onTap: () => onTap(e),
            ))
        .toList();
  }
}
