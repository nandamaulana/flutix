part of 'pages.dart';

class Wrapper extends StatelessWidget {
  const Wrapper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    User? user = Provider.of<User?>(context);
    if (user == null) {
      if (!(prevPageEvent is GoToSplashPage)) {
        prevPageEvent = GoToSplashPage();
        context.read<PageBloc>().add(GoToSplashPage());
      }
    } else {
      if (!(prevPageEvent is GoToMainPage)) {
        context.read<UserBloc>().add(LoadUser(user.uid));

        prevPageEvent = GoToMainPage();
        context.read<PageBloc>().add(GoToMainPage());
      }
    }
    return BlocBuilder<PageBloc, PageState>(
        builder: (_, state) => AnimatedSwitcher(
            duration: Duration(milliseconds: 250), child: getPage(state)));
  }

  Widget getPage(PageState state) {
    if (state is OnSplashPage)
      return SplashPage();
    else if (state is OnLoginPage)
      return SignInPage();
    else if (state is OnSignUpPage)
      return SignUpPage(registrationData: state.registrationData);
    else if (state is OnPreferencePage)
      return PreferencePage(registrationData: state.registrationData);
    else if (state is OnAccountConfirmationPage)
      return AccountConfirmationPage(state.registrationData);
    else
      return MainPage();
  }
}
