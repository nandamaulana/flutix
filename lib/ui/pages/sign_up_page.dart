part of 'pages.dart';

class SignUpPage extends StatefulWidget {
  final RegistrationData registrationData;
  const SignUpPage({Key? key, required this.registrationData})
      : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  bool isEmailExist = false;

  @override
  void initState() {
    super.initState();

    nameController.text = widget.registrationData.name ?? "";
    emailController.text = widget.registrationData.email ?? "";
  }

  @override
  Widget build(BuildContext context) {
    context.read<ThemeBloc>().add(ChangeTheme(ThemeData().copyWith(
        inputDecorationTheme: InputDecorationTheme(
            labelStyle: TextStyle(color: Colors.grey),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: accentColor1, width: 2))))));
    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(GoToSplashPage());
        return Future.value(false);
      },
      child: Scaffold(
        body: Container(
          color: Colors.white,
          padding: EdgeInsets.symmetric(horizontal: defaultMargin),
          child: ListView(
            children: [
              Column(
                children: [
                  Container(
                    height: 56,
                    margin: EdgeInsets.only(top: 20, bottom: 22),
                    child: Stack(
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              customBorder: CircleBorder(),
                              onTap: () {
                                context.read<PageBloc>().add(GoToSplashPage());
                              },
                              child: Icon(Icons.arrow_back),
                            ),
                          ),
                        ),
                        Align(
                            alignment: Alignment.center,
                            child: Text("Create New \nYour Account",
                                style: blackTextFont.copyWith(
                                    fontSize: 20, fontWeight: FontWeight.w600),
                                textAlign: TextAlign.center)),
                      ],
                    ),
                  ),
                  Container(
                      width: 90,
                      height: 104,
                      child: Stack(
                        children: [
                          Container(
                            height: 90,
                            width: 90,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: ((widget.registrationData
                                                    .profileImage ??
                                                "") ==
                                            "")
                                        ? AssetImage("assets/user_pic.png")
                                            as ImageProvider
                                        : FileImage(widget
                                            .registrationData.profileImage!),
                                    fit: BoxFit.cover)),
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Material(
                              color: Colors.transparent,
                              child: Ink(
                                  height: 28,
                                  width: 28,
                                  child: InkWell(
                                    customBorder: CircleBorder(),
                                    onTap: () async {
                                      if (((widget.registrationData
                                                  .profileImage ??
                                              "") ==
                                          "")) {
                                        widget.registrationData.profileImage =
                                            await getImage();
                                      } else {
                                        widget.registrationData.profileImage =
                                            null;
                                      }
                                      setState(() {});
                                    },
                                  ),
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage(((widget
                                                        .registrationData
                                                        .profileImage ??
                                                    "") ==
                                                "")
                                            ? "assets/btn_add_photo.png"
                                            : "assets/btn_del_photo.png")),
                                  )),
                            ),
                          )
                        ],
                      )),
                  SizedBox(
                    height: 36,
                  ),
                  TextField(
                    controller: nameController,
                    textInputAction: TextInputAction.next,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: "Full Name",
                        hintText: "Full Name"),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Focus(
                    skipTraversal: true,
                    onFocusChange: (hasFocus) async {
                      setState(() {
                        isEmailExist = false;
                      });
                      if (!hasFocus &&
                          EmailValidator.validate(
                              emailController.text.trim())) {
                        bool isExists = await AuthServices.checkEmailExists(
                            emailController.text.trim());
                        if (!isExists) {
                          alertEmailAlreadyUse.showMessage(
                              context: context, duration: 3000);
                          setState(() {
                            isEmailExist = true;
                          });
                        }
                      }
                    },
                    child: TextField(
                      controller: emailController,
                      textInputAction: TextInputAction.next,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10)),
                          labelText: "Email",
                          hintText: "Email"),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  TextField(
                    controller: passwordController,
                    textInputAction: TextInputAction.next,
                    obscureText: true,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: "Password",
                        hintText: "Password"),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  TextField(
                    controller: confirmPasswordController,
                    textInputAction: TextInputAction.done,
                    obscureText: true,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: "Confirm Password",
                        hintText: "Confirm Password"),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  FloatingActionButton(
                    onPressed: () {
                      if (nameController.text.trim() == "" ||
                          emailController.text.trim() == "" ||
                          passwordController.text.trim() == "" ||
                          confirmPasswordController.text.trim() == "") {
                        alertFillallField.showMessage(context: context);
                      } else if (passwordController.text.trim() !=
                          confirmPasswordController.text.trim()) {
                        alertMissmatchPassword.showMessage(context: context);
                      } else if (passwordController.text.trim().length < 6) {
                        alertPassMoreThan.showMessage(context: context);
                      } else if (!EmailValidator.validate(
                          emailController.text.trim())) {
                        alertFormatEmailAddress.showMessage(context: context);
                      } else if (isEmailExist) {
                        alertEmailAlreadyUse.showMessage(
                            context: context, duration: 3000);
                      } else {
                        widget.registrationData.name =
                            nameController.text.trim();
                        widget.registrationData.email =
                            emailController.text.trim();
                        widget.registrationData.password =
                            passwordController.text.trim();

                        context
                            .read<PageBloc>()
                            .add(GoToPreferencePage(widget.registrationData));
                      }
                    },
                    child: Icon(Icons.arrow_forward),
                    backgroundColor: mainColor,
                    elevation: 0,
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
