part of 'services.dart';

class UserServices {
  static CollectionReference _userCollection =
      FirebaseFirestore.instance.collection(userFirestoreCollection);

  static Future<void> updateUser(FlutixUser flutixUser) async {
    _userCollection.doc(flutixUser.id).set({
      userEmailData: flutixUser.email,
      userNameData: flutixUser.name,
      userProfilePictureData: flutixUser.profilePicture ?? "",
      userSelectedGenresData: flutixUser.selectedGenres ?? "",
      userSelectedLanguageData: flutixUser.selectedLanguage,
      userBalanceData: flutixUser.balance,
    });
  }

  static Future<FlutixUser> getUser(String id) async {
    DocumentSnapshot snapshot = await _userCollection.doc(id).get();

    return FlutixUser(
      id,
      snapshot.get(userEmailData),
      name: snapshot.get(userNameData),
      profilePicture: snapshot.get(userProfilePictureData),
      selectedGenres: (snapshot.get(userSelectedGenresData) as List)
          .map((e) => e.toString())
          .toList(),
      selectedLanguage: snapshot.get(userSelectedLanguageData),
      balance: snapshot.get(userBalanceData),
    );
  }
}
