part of 'services.dart';

class AuthServices {
  static FirebaseAuth auth = FirebaseAuth.instance;
  static Future<SignInSignUpResult> signUp(String email, String password,
      String name, List<String> selectedGenres, String selectedLanguage) async {
    try {
      UserCredential result = await auth.createUserWithEmailAndPassword(
          email: email, password: password);
      FlutixUser user = result.user!.convertToUser(
          name: name,
          selectedGenres: selectedGenres,
          selectedLanguage: selectedLanguage);

      await UserServices.updateUser(user);
      return SignInSignUpResult(user: user);
    } catch (e) {
      return SignInSignUpResult(message: e.toString());
    }
  }

  static Future<bool> checkEmailExists(String email) async {
    List<String> result = await auth.fetchSignInMethodsForEmail(email);
    return result.isEmpty;
  }

  static Future<SignInSignUpResult> signIn(
      String email, String password) async {
    try {
      UserCredential result = await auth.signInWithEmailAndPassword(
          email: email, password: password);

      FlutixUser? user = await result.user?.fromFirestore();
      if (user != null) {
        return SignInSignUpResult(user: user);
      }
      return SignInSignUpResult(message: "user not found");
    } on FirebaseAuthException catch (e) {
      return SignInSignUpResult(message: e.message);
    } catch (e) {
      return SignInSignUpResult(message: e.toString());
    }
  }

  static Future<void> signOut() async {
    await auth.signOut();
  }

  static Stream<User?> get userStream => auth.authStateChanges();
}

class SignInSignUpResult {
  final FlutixUser? user;
  final String? message;

  SignInSignUpResult({this.user, this.message});
}
