part of 'theme_bloc.dart';

class ThemeEvent extends Equatable {
  const ThemeEvent();

  @override
  List<Object> get props => [];
}

class ChangeTheme extends ThemeEvent {
  final ThemeData themeData;

  const ChangeTheme(this.themeData);

  @override
  List<Object> get props => [themeData];
}
