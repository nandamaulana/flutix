import 'package:firebase_core/firebase_core.dart';
import 'package:flutix/bloc/bloc.dart';
import 'package:flutix/bloc/movie_bloc.dart';
import 'package:flutix/bloc/theme_bloc.dart';
import 'package:flutix/services/services.dart';
import 'package:flutix/shared/shared.dart';
import 'package:flutix/ui/pages/pages.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await SentryFlutter.init(
    (options) {
      options.dsn =
          'https://6ea6c7ae242e4e518147521fb84a984b@o1057977.ingest.sentry.io/6045174';
    },
    appRunner: () => runApp(MyApp()),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: accentColor1));
    return StreamProvider.value(
        value: AuthServices.userStream,
        initialData: null,
        child: MultiBlocProvider(
            providers: [
              BlocProvider(create: (_) => PageBloc()),
              BlocProvider(create: (_) => UserBloc()),
              BlocProvider(create: (_) => ThemeBloc()),
              BlocProvider(create: (_) => MovieBloc()..add(FetchMovies())),
            ],
            child: BlocBuilder<ThemeBloc, ThemeState>(
              builder: (_, themeState) {
                return MaterialApp(
                    theme: themeState.themeData,
                    debugShowCheckedModeBanner: false,
                    home: Wrapper());
              },
            )));
  }
}
