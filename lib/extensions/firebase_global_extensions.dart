part of 'extensions.dart';

extension FirebaseGlobalExtensions on String? {
  showMessage({required BuildContext context, int duration = 1500}) {
    Flushbar(
      duration: Duration(milliseconds: duration),
      flushbarPosition: FlushbarPosition.TOP,
      backgroundColor: wildWatermelon,
      message: this,
    ).show(context);
  }
}
