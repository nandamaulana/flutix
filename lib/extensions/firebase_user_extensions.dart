part of 'extensions.dart';

extension FirebaseUserExtension on User {
  FlutixUser convertToUser(
          {String name = "No Name",
          List<String> selectedGenres = const [],
          String selectedLanguage = "English",
          int balance = 50000}) =>
      FlutixUser(this.uid, this.email,
          name: name,
          balance: balance,
          selectedGenres: selectedGenres,
          selectedLanguage: selectedLanguage);

  Future<FlutixUser> fromFirestore() async =>
      await UserServices.getUser(this.uid);
}
