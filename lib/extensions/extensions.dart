import 'package:another_flushbar/flushbar.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutix/models/models.dart';
import 'package:flutix/services/services.dart';
import 'package:flutix/shared/shared.dart';
import 'package:flutter/cupertino.dart';

part 'firebase_user_extensions.dart';
part 'firebase_global_extensions.dart';
